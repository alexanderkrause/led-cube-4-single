#ifndef PHYSICAL_H
#define PHYSICAL_H

#define COLUMN0   52 
#define COLUMN1   50
#define COLUMN2   48
#define COLUMN3   46
#define COLUMN4   44
#define COLUMN5   42
#define COLUMN6   40
#define COLUMN7   38
#define COLUMN8   36
#define COLUMN9   34
#define COLUMN10  32
#define COLUMN11  30
#define COLUMN12  28
#define COLUMN13  26
#define COLUMN14  24
#define COLUMN15  22

#define LAYER0 47
#define LAYER1 49
#define LAYER2 51
#define LAYER3 53

#include <Vector.h>
#include <Arduino.h>


/*
Anschlüsse links
Ansicht von oben
Layer 0 unten, Layer 3 oben

Y 
3 |  12  13  14  15
2 |   8   9  10  11
1 |   4   5   6   7
0 |   0   1   2   3
  |_________________
X     0   1   2   3

*/

constexpr uint8_t LED_COLUMN[16] = {COLUMN0, COLUMN1, COLUMN2, COLUMN3, COLUMN4, COLUMN5, COLUMN6, COLUMN7, COLUMN8, COLUMN9, COLUMN10, COLUMN11, COLUMN12, COLUMN13, COLUMN14, COLUMN15};
constexpr uint8_t LED_LAYER[4] = {LAYER0, LAYER1, LAYER2, LAYER3};


void setLight(uint8_t width, uint8_t depth, uint8_t layer, bool state);




#endif