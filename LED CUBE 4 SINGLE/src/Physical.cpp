#include "Physical.h"

void setLight(uint8_t width, uint8_t depth, uint8_t layer, bool state) {
  uint8_t column = depth * 4 + width;
  if (state) {
    digitalWrite(LED_LAYER[layer], LOW);
    digitalWrite(LED_COLUMN[column], HIGH);
  } else {
    digitalWrite(LED_LAYER[layer], HIGH);
    digitalWrite(LED_COLUMN[column], LOW);
  }
}