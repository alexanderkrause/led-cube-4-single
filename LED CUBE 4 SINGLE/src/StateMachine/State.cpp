#include "State.h"
void State::ledOnOff(uint8_t width, uint8_t depth, uint8_t heigth) {
  setLight(width, depth, heigth, true);
  setLight(width, depth, heigth, false);
}