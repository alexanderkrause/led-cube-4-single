#include "RandomDot.h"

RandomDot::RandomDot(/* args */)
{
}

RandomDot::~RandomDot()
{
}

void RandomDot::shine(uint32_t lengthInMilliseconds) {
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    /*
    uint8_t x = random(4);
    uint8_t y = random(4);
    uint8_t z = random(4);
    setLight(x, y, z, true);
    delay(lengthInMilliseconds);
    setLight(x, y, z, false);
    */
    uint8_t val = random(64);
    setLight(val % 4, (val >> 2) % 4, (val >> 4) % 4, true);
    delay(lengthInMilliseconds);
    setLight(val % 4, (val >> 2) % 4, (val >> 4) % 4, false);
  }
}

String RandomDot::toString() {
  return "RandomDot";
}