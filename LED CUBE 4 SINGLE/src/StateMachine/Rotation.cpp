#include "Rotation.h"

Rotation::Rotation(/* args */)
{
}

Rotation::~Rotation()
{
}

void Rotation::shine(uint32_t lengthInMilliseconds) {
  
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    for (size_t i = 0; i < 9; ++i)
    {
      illuminatePlane(Axis::x, i, Position::quadrant1, lengthInMilliseconds);
      
    }
  }
}

void Rotation::illuminatePlane(Axis axis, uint8_t step, Position quadrant, uint32_t lengthInMilliseconds) {
  unsigned long start = millis();
  switch (axis)
  {
  case x:
    switch (quadrant)
    {
    case quadrant1:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, 3);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 3, 2);
            ledOnOff(i, 0, 3);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 2);
            ledOnOff(i, 0, 3);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 3, 1);
            ledOnOff(i, 0, 3);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, 3 - j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 0);
            ledOnOff(i, 0, 3);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 1);
            ledOnOff(i, 0, 3);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 0);
            ledOnOff(i, 0, 3);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, 0, j);
            }
          }
        }
        break;
      }
      break;
    case quadrant2:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, 3, j);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 0);
            ledOnOff(i, 3, 3);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 1);
            ledOnOff(i, 3, 3);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 0);
            ledOnOff(i, 3, 3);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 0, 1);
            ledOnOff(i, 3, 3);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 2);
            ledOnOff(i, 3, 3);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 0, 2);
            ledOnOff(i, 3, 3);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, 3);
            }
          }
        }
        break;
      }
      break;
    case quadrant3:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, 0);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 0, 1);
            ledOnOff(i, 3, 0);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 1);
            ledOnOff(i, 3, 0);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 0, 2);
            ledOnOff(i, 3, 0);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, 3 - j, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 3);
            ledOnOff(i, 3, 0);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 2);
            ledOnOff(i, 3, 0);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 3);
            ledOnOff(i, 3, 0);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, 3, j);
            }
          }
        }
        break;
      }
      break;
    case quadrant4:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, 0, j);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 3);
            ledOnOff(i, 0, 0);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 1, 2);
            ledOnOff(i, 0, 0);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 3);
            ledOnOff(i, 0, 0);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 3, 2);
            ledOnOff(i, 0, 0);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 2, 1);
            ledOnOff(i, 0, 0);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(i, 3, 1);
            ledOnOff(i, 0, 0);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(i, j, 0);
            }
          }
        }
        break;
      }
      break;
    }
    break;
  case y:
    switch (quadrant)
    {
    case quadrant1:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, i, 3);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, i, 2);
            ledOnOff(3, i, 3);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 2);
            ledOnOff(3, i, 3);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, i, 1);
            ledOnOff(3, i, 3);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, i, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 0);
            ledOnOff(3, i, 3);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 1);
            ledOnOff(3, i, 3);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 0);
            ledOnOff(3, i, 3);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3, i, j);
            }
          }
        }
        break;
      }
      break;
    case quadrant2:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(0, i, j);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 0);
            ledOnOff(0, i, 3);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 1);
            ledOnOff(0, i, 3);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 0);
            ledOnOff(0, i, 3);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3 - j, i, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, i, 1);
            ledOnOff(0, i, 3);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 2);
            ledOnOff(0, i, 3);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, i, 2);
            ledOnOff(0, i, 3);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, i, 3);
            }
          }
        }
        break;
      }
      break;
    case quadrant3:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, i, 0);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, i, 1);
            ledOnOff(0, i, 0);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 1);
            ledOnOff(0, i, 0);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, i, 2);
            ledOnOff(0, i, 0);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, i, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 3);
            ledOnOff(0, i, 0);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 2);
            ledOnOff(0, i, 0);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 3);
            ledOnOff(0, i, 0);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(0, i, j);
            }
          }
        }
        break;
      }
      break;
    case quadrant4:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3, i, j);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 3);
            ledOnOff(3, i, 0);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, i, 2);
            ledOnOff(3, i, 0);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 3);
            ledOnOff(3, i, 0);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3 - j, i, j);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, i, 2);
            ledOnOff(3, i, 0);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, i, 1);
            ledOnOff(3, i, 0);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, i, 1);
            ledOnOff(3, i, 0);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, i, 0);
            }
          }
        }
        break;
      }
      break;
    }
    break;
  case z:
    switch (quadrant)
    {
    case quadrant1:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, 3, i);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, 2, i);
            ledOnOff(3, 3, i);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 2, i);
            ledOnOff(3, 3, i);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, 1, i);
            ledOnOff(3, 3, i);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, j, i);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 0, i);
            ledOnOff(3, 3, i);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 1, i);
            ledOnOff(3, 3, i);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 0, i);
            ledOnOff(3, 3, i);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3, j, i);
            }
          }
        }
        break;
      }
      break;
    case quadrant2:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(0, j, i);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 0, i);
            ledOnOff(0, 3, i);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 1, i);
            ledOnOff(0, 3, i);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 0, i);
            ledOnOff(0, 3, i);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3 - j, j, i);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, 1, i);
            ledOnOff(0, 3, i);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 2, i);
            ledOnOff(0, 3, i);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, 2, i);
            ledOnOff(0, 3, i);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, 3, i);
            }
          }
        }
        break;
      }
      break;
    case quadrant3:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, 0, i);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, 1, i);
            ledOnOff(0, 0, i);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 1, i);
            ledOnOff(0, 0, i);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(3, 2, i);
            ledOnOff(0, 0, i);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, j, i);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 3, i);
            ledOnOff(0, 0, i);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 2, i);
            ledOnOff(0, 0, i);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 3, i);
            ledOnOff(0, 0, i);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(0, j, i);
            }
          }
        }
        break;
      }
      break;
    case quadrant4:
      switch (step)
      {
      case 0:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3, j, i);
            }
          }
        }
        break;
      case 1:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 3, i);
            ledOnOff(3, 0, i);
          }
        }
        break;
      case 2:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(2, 2, i);
            ledOnOff(3, 0, i);
          }
        }
        break;
      case 3:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 3, i);
            ledOnOff(3, 0, i);
          }
        }
        break;
      case 4:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(3 - j, j, i);
            }
          }
        }
        break;
      case 5:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, 2, i);
            ledOnOff(3, 0, i);
          }
        }
        break;
      case 6:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(1, 1, i);
            ledOnOff(3, 0, i);
          }
        }
        break;
      case 7:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            ledOnOff(0, 1, i);
            ledOnOff(3, 0, i);
          }
        }
        break;
      case 8:
        while ((millis() - start) < lengthInMilliseconds)
        {
          for (size_t i = 0; i < 4; ++i)
          {
            for (size_t j = 0; j < 4; ++j)
            {
              ledOnOff(j, 0, i);
            }
          }
        }
        break;
      }
      break;
    }
    break;
  }
}


String Rotation::toString() {
  return "Rotation";
}
