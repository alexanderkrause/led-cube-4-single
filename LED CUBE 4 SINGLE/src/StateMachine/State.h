#ifndef STATE_H
#define STATE_H

#include <Arduino.h>
#include "Physical.h"

class State
{
private:
  /* data */
public:
  virtual String toString() = 0;
  virtual void shine(uint32_t lengthInMilliseconds) = 0;
  void ledOnOff(uint8_t width, uint8_t depth, uint8_t heigth);
};




#endif