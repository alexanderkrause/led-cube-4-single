#ifndef ROTATION_H
#define ROTATION_H

#include "State.h"

class Rotation : public State
{
private:
  /* data */
public:
  enum Axis {
    x = 0,
    y = 1,
    z = 2
  };

  enum Position {
    quadrant1,
    quadrant2,
    quadrant3,
    quadrant4
  };

  Rotation(/* args */);
  ~Rotation();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
  void illuminatePlane(Axis axis, uint8_t step, Position quadrant, uint32_t lengthInMilliseconds);
};


#endif