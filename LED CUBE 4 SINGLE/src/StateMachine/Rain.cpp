#include "Rain.h"

void Rain::printList(LinkedList<Rain::Drop> list) {
  Serial.print("Content: ");
  Serial.println(list.getSize());
  for (size_t i = 0; i < list.getSize(); ++i)
  {
    Serial.print("Printlist iteration: ");
    Serial.println(i);
    Rain::Drop* drop;
    drop = list.at(i);
    Serial.print("X: ");
    Serial.println(drop->x);
    Serial.print("Y: ");
    Serial.println(drop->y);
    Serial.print("Z: ");
    Serial.println(drop->z);
  }
  Serial.println("End printlist");
}

Rain::Rain(/* args */)
{
}

Rain::~Rain()
{
}

void Rain::shine(uint32_t lengthInMilliseconds) {
  LinkedList<Drop> drops;
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    Serial.println("New Iteration");
    
    // generate new drops
    uint8_t newDropCount = random(1, 5);
    Serial.println(newDropCount);
    Serial.println(drops.getSize());
    
    for (size_t i = 0; i < newDropCount; ++i)
    {
      Drop* drop = new Drop();
      drop->x = random(4);
      drop->y = random(4);
      drop->z = 3;
      drops.push(drop);
    }
    
    Serial.println("Create");
    printList(drops);
    Serial.println("-------------");
    delay(1000);

    // draw drops
    //unsigned long startDraw = millis();
    /*
    while ((millis() - startDraw) < 250)
    {
      for (size_t i = 0; i < drops.getSize(); ++i)
      {
        Drop* drop;
        drop = drops.at(i);
        ledOnOff(drop->x, drop->y, drop->z);
      }
    }
    */
    Drop* drop;
    drop = drops.at(0);
    Serial.println(drop->x);
    //setLight(drop->x, drop->y, drop->z, true);
    delay(1000);
    /*

    Serial.println("Draw");
    printList(drops);
    
    delay(1000);

    // advance drops downwards
    for (size_t i = 0; i < drops.getSize(); ++i)
    {
      Drop* drop;
      drop = drops.at(i);
      drop->z = drop->z - 1;
    }

    Serial.println("Advance");
    printList(drops);
    delay(1000);
    
    Drop* drop;
    drop = drops.at(0);
    while (drop->z < 0)
    {
      drops.pop();
      delete drop;
      drop = drops.at(0);
    }

    Serial.println("Remove");
    printList(drops);
    delay(1000);

    */
    Serial.println("End Iteration");
  }
}

String Rain::toString() {
  return "Rain";
}