#ifndef FULL_H
#define FULL_H

#include "State.h"

class Full : public State
{
private:
  /* data */
public:
  Full(/* args */);
  ~Full();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
};


#endif