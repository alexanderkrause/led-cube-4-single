#ifndef EDGE_H
#define EDGE_H

#include "State.h"

class Edge : public State
{
private:
  /* data */
public:
  Edge(/* args */);
  ~Edge();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
};


#endif