#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "State.h"

/**
 * @brief Singleton state machine
 * 
 */
class StateMachine
{
private:
  StateMachine(/* args */){};
  State* state;

public:
  static StateMachine& getInstance()
	{
		static StateMachine instance;
		return instance;
	}

  StateMachine(StateMachine const&) = delete;
  void operator=(StateMachine const&)  = delete;
  void shine();
  void shine(uint32_t lengthInMilliseconds);
  void resetLighting();
  void setState(State* state);
  void setState(State* state, uint32_t lengthInMilliseconds);

};



#endif