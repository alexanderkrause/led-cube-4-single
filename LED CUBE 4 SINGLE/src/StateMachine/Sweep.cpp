#include "Sweep.h"

Sweep::Sweep(/* args */)
{
}

Sweep::~Sweep()
{
}

void Sweep::shine(uint32_t lengthInMilliseconds) {
  
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    sweepPlane(true, Plane::x, lengthInMilliseconds);
    sweepPlane(false, Plane::x, lengthInMilliseconds);

    sweepPlane(true, Plane::y, lengthInMilliseconds);
    sweepPlane(false, Plane::y, lengthInMilliseconds);

    sweepPlane(true, Plane::z, lengthInMilliseconds);
    sweepPlane(false, Plane::z, lengthInMilliseconds);
  }
  
}

String Sweep::toString() {
  return "Sweep";
}

void Sweep::illuminatePlane(Plane plane, uint8_t value, uint32_t lengthInMilliseconds) {
  unsigned long start = millis();
  switch (plane)
  {
  case x:
    while ((millis() - start) < lengthInMilliseconds)
    {
      for (size_t i = 0; i < 4; ++i)
      {
        for (size_t j = 0; j < 4; ++j)
        {
          ledOnOff(value, i, j);
        }
      }
    }
    break;
  
  case y:
    while ((millis() - start) < lengthInMilliseconds)
    {
      for (size_t i = 0; i < 4; ++i)
      {
        for (size_t j = 0; j < 4; ++j)
        {
          ledOnOff(i, value, j);
        }
      }
    }
    break;

  case z:
    while ((millis() - start) < lengthInMilliseconds)
    {
      for (size_t i = 0; i < 4; ++i)
      {
        for (size_t j = 0; j < 4; ++j)
        {
          ledOnOff(i, j, value);
        }
      }
    }
    break;
  }  
}

void Sweep::sweepPlane(bool direction, Plane plane, uint32_t lengthInMilliseconds) {
  if (direction)
  { 
    for (size_t i = 0; i < 4; ++i)
    {
      illuminatePlane(plane, i, lengthInMilliseconds >> 2);
    }
  } else {
    for (int i = 3; i >= 0; --i)
    {
      illuminatePlane(plane, i, lengthInMilliseconds >> 2);
    }
  }
}