#ifndef SWEEP_H
#define SWEEP_H

#include "State.h"

class Sweep : public State
{
private:
  /* data */
public:
  enum Plane {
    x = 0,
    y = 1,
    z = 2
  };

  Sweep(/* args */);
  ~Sweep();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
  void sweepPlane(bool direction, Plane plane, uint32_t lengthInMilliseconds);
  void illuminatePlane(Plane plane, uint8_t value, uint32_t lengthInMilliseconds);
};


#endif