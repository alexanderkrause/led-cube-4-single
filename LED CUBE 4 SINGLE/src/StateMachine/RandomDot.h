#ifndef RANDOM_DOT_H
#define RANDOM_DOT_H

#include "State.h"

class RandomDot : public State
{
private:
  /* data */
public:
  RandomDot(/* args */);
  ~RandomDot();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
};

#endif