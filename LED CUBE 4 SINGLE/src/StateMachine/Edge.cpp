#include "Edge.h"

Edge::Edge(/* args */)
{
}

Edge::~Edge()
{
}

void Edge::shine(uint32_t lengthInMilliseconds) {
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    ledOnOff(0, 0, 0);
    ledOnOff(0, 0, 1);
    ledOnOff(0, 0, 2);
    ledOnOff(0, 0, 3);

    ledOnOff(3, 0, 0);
    ledOnOff(3, 0, 1);
    ledOnOff(3, 0, 2);
    ledOnOff(3, 0, 3);

    ledOnOff(0, 3, 0);
    ledOnOff(0, 3, 1);
    ledOnOff(0, 3, 2);
    ledOnOff(0, 3, 3);

    ledOnOff(3, 3, 0);
    ledOnOff(3, 3, 1);
    ledOnOff(3, 3, 2);
    ledOnOff(3, 3, 3);

    ledOnOff(0, 1, 0);
    ledOnOff(0, 2, 0);

    ledOnOff(3, 1, 0);
    ledOnOff(3, 2, 0);

    ledOnOff(1, 0, 0);
    ledOnOff(2, 0, 0);

    ledOnOff(1, 3, 0);
    ledOnOff(2, 3, 0);

    ledOnOff(0, 1, 3);
    ledOnOff(0, 2, 3);

    ledOnOff(3, 1, 3);
    ledOnOff(3, 2, 3);

    ledOnOff(1, 0, 3);
    ledOnOff(2, 0, 3);

    ledOnOff(1, 3, 3);
    ledOnOff(2, 3, 3);
  }
}

String Edge::toString() {
  return "Edge";
}