#include "Corner.h"

Corner::Corner(/* args */)
{
}

Corner::~Corner()
{
}

void Corner::shine(uint32_t lengthInMilliseconds) {
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    ledOnOff(0, 0, 0);
    ledOnOff(0, 0, 3);
    ledOnOff(0, 3, 0);
    ledOnOff(0, 3, 3);
    ledOnOff(3, 0, 0);
    ledOnOff(3, 0, 3);
    ledOnOff(3, 3, 0);
    ledOnOff(3, 3, 3);
  }
}

String Corner::toString() {
  return "Corner";
}