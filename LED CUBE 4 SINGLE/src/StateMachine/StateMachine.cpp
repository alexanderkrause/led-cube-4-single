#include "StateMachine.h"

void StateMachine::setState(State* state) {
  setState(state, 100);
}

void StateMachine::setState(State* state, uint32_t lengthInMilliseconds) {
  this->state = state;
  resetLighting();
  this->shine(lengthInMilliseconds);
}


void StateMachine::resetLighting() {
  for (uint8_t i = 0; i < 16; ++i) {
    digitalWrite(LED_COLUMN[i], LOW);
  }

  for (uint8_t i = 0; i < 4; ++i) {
    digitalWrite(LED_LAYER[i], HIGH);
  }
}

void StateMachine::shine() {
  shine(100);
}

void StateMachine::shine(uint32_t lengthInMilliseconds) {
  this->state->shine(lengthInMilliseconds);
}