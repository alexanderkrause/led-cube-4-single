#ifndef CORNER_H
#define CORNER_H

#include "State.h"

class Corner : public State
{
private:
  /* data */
public:
  Corner(/* args */);
  ~Corner();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
};


#endif