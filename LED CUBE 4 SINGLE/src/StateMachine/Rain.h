#ifndef RAIN_H
#define RAIN_H

#include "State.h"
//#include "./../LinkedList/LinkedList.h"
#include "LinkedList/LinkedList.h"

class Rain : public State
{

public:
  Rain(/* args */);
  ~Rain();
  void shine(uint32_t lengthInMilliseconds);
  String toString();
  class Drop {
    public:
      uint8_t x;
      uint8_t y;
      int8_t z;
  };

  private:
    /* data */
    void printList(LinkedList<Rain::Drop> list);
};

#endif