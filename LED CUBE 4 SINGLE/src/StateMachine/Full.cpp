#include "Full.h"

Full::Full(/* args */)
{
}

Full::~Full()
{
}

void Full::shine(uint32_t lengthInMilliseconds) {
  unsigned long start = millis();
  while ((millis() - start) < lengthInMilliseconds)
  {
    for (size_t i = 0; i < 4; ++i)
    {
      for (size_t j = 0; j < 4; ++j)
      {
        for (size_t k = 0; k < 4; ++k)
        {
          ledOnOff(i, j, k);
        }
      }
    } 
  }
}

String Full::toString() {
  return "Full";
}