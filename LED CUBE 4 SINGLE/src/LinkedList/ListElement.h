#ifndef LIST_ELEMENT_H
#define LIST_ELEMENT_H

template <class T>
class ListElement
{
private:
  /* data */
  T* data;
  ListElement<T>* nextElement = nullptr;
public:
  ListElement(T* data);
  ~ListElement() {delete data;};
  void setNextElement(ListElement<T>* next);
  T* getData();
  ListElement<T>* getNextElement();
};


template <class T>
ListElement<T>::ListElement(T* data) : data(data)
{
}

template <class T>
void ListElement<T>::setNextElement(ListElement<T>* next) {
  this->nextElement = next;
}

template <class T>
T* ListElement<T>::getData() {
  return data;
}

template <class T>
ListElement<T>* ListElement<T>::getNextElement() {
  return nextElement;
}


#endif