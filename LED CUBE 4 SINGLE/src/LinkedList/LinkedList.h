#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "ListElement.h"

template <class T>
class LinkedList
{
private:
  ListElement<T>* firstElement = nullptr;
  ListElement<T>* lastElement = nullptr;
  uint32_t numberOfElements = 0;
public:
  LinkedList(/* args */);
  ~LinkedList();
  void push(T* element);
  T* pop();
  T* at(uint32_t index);
  uint32_t getSize();
};

template <class T>
uint32_t LinkedList<T>::getSize() {
  return numberOfElements;
}

template <class T>
LinkedList<T>::LinkedList(/* args */)
{
}

template <class T>
LinkedList<T>::~LinkedList(/* args */)
{
  while (numberOfElements > 0)
  {
    T* elem = pop();
    delete elem;
  }
  
}

template <class T>
void LinkedList<T>::push(T* element) {
  ListElement<T>* newElement = new ListElement<T>(element);

  ListElement<T>* prevLast = lastElement;
  lastElement = newElement;
  if (numberOfElements == 0)
  {
    firstElement = newElement;
  } else
  {
    prevLast->setNextElement(newElement);
  }
  
  numberOfElements += 1;
}

template <class T>
T* LinkedList<T>::pop() {
  
  if (numberOfElements > 0)
  {
    T* data = firstElement->getData();
    ListElement<T>* prevFirst = firstElement;
    firstElement = prevFirst->getNextElement();
    delete(prevFirst);
    numberOfElements -= 1;
    return data;
  } else {
    return NULL;
  }
}

template <class T>
T* LinkedList<T>::at(uint32_t index) {
  if (index < numberOfElements)
  {
    ListElement<T>* elem = firstElement;
    for (size_t i = 0; i < index; ++i)
    {
      elem = elem->getNextElement();
    }
    
    return elem->getData();
  } else {
    return NULL;
  }
  
}

#endif