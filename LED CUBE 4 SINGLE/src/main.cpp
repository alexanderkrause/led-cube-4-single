#include <Arduino.h>
#include "Physical.h"

#include "StateMachine/StateMachine.h"
#include "StateMachine/Full.h"
#include "StateMachine/Edge.h"
#include "StateMachine/RandomDot.h"
#include "StateMachine/Corner.h"
#include "StateMachine/Sweep.h"
#include "StateMachine/Rotation.h"
#include "StateMachine/Rain.h"

void setup() {
  // put your setup code here, to run once:

  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("* * * * * * * * * * *");

  for (int8_t i = 0; i < 16; ++i) {
    pinMode(LED_COLUMN[i], OUTPUT);
    digitalWrite(LED_COLUMN[i], LOW);
  }

  for (int8_t i = 0; i < 4; ++i) {
    pinMode(LED_LAYER[i], OUTPUT);
    digitalWrite(LED_LAYER[i], HIGH);
  }
  
  
  for (int8_t i = 0; i < 4; ++i) {
    digitalWrite(LED_LAYER[i], LOW);
    for (int8_t j = 0; j < 16; ++j) {
      digitalWrite(LED_COLUMN[j], HIGH);
      delay(10);
      digitalWrite(LED_COLUMN[j], LOW);
    }
    digitalWrite(LED_LAYER[i], HIGH);
  }

  randomSeed(analogRead(0));
}

State* full = new Full();
State* edge = new Edge();
State* randomDot = new RandomDot();
State* corner = new Corner();
State* sweep = new Sweep();
State* rotation = new Rotation();
State* rain = new Rain();

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Loop Iteration Main");
  
  StateMachine::getInstance().setState(full);
  StateMachine::getInstance().setState(edge);
  StateMachine::getInstance().setState(randomDot, 50);
  StateMachine::getInstance().setState(corner, 50);
  StateMachine::getInstance().setState(sweep, 1000);
  StateMachine::getInstance().setState(rotation, 100);
  //StateMachine::getInstance().setState(rain, UINT32_MAX);
}